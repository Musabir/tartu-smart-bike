package ee.ut.its.tartusmartbike.dock;

import com.google.gson.Gson;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ee.ut.its.tartusmartbike.api.DockApi;
import ee.ut.its.tartusmartbike.model.Dock;
import ee.ut.its.tartusmartbike.model.DockListRequest;
import ee.ut.its.tartusmartbike.services.DockService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

class DockServiceTest {

    private DockService dockService;

    @Mock
    private DockApi dockApi;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        dockService = new DockService(dockApi);
    }

    @Test
    void all() throws IOException {
        //given
        doReturn(mockedDocks()).when(dockApi).all();

        //when
        List<DockListRequest.DockResponse> docks = dockService.getAllDocks();

        //then
        assertThat(docks).hasSize(4);
        verifyCoordinates(docks);
        verifyAddress(docks);
        verifyNames(docks);
    }

    private void verifyCoordinates(List<DockListRequest.DockResponse> allDocks) {
        Double[][] expectedPoints = new Double[][]{
                {58.38079076345506, 26.724423279705707},
                {58.36924551095159, 26.74009749648303},
                {58.35511411318977, 26.709266920776592},
                {58.37249777387615, 26.77965084014625}
        };

        for (int i = 0; i < allDocks.size(); i++) {
            assertThat(allDocks.get(i).getAreaCentroid().getLatitude()).isEqualTo(expectedPoints[i][0]);
            assertThat(allDocks.get(i).getAreaCentroid().getLongitude()).isEqualTo(expectedPoints[i][1]);
        }
    }

    private void verifyAddress(List<DockListRequest.DockResponse> allDocks) {
        String[] expectedNames = new String[]{"Magistri", "Sõpruse sild", "Viie tee rist", "Mõisavahe"};
        for (int i = 0; i < allDocks.size(); i++) {
            assertThat(allDocks.get(i).getAddress()).isEqualTo(expectedNames[i]);
        }
    }

    private void verifyNames(List<DockListRequest.DockResponse> allDocks) {
        String[] expectedNames = new String[]{"Magistri", "Sõpruse sild", "Viie tee rist", "Mõisavahe"};
        for (int i = 0; i < allDocks.size(); i++) {
            assertThat(allDocks.get(i).getName()).isEqualTo(expectedNames[i]);
        }
    }

    private List<DockListRequest.DockResponse> mockedDocks() {
        Gson gson = new Gson();
        return gson.fromJson(jsonResponse(), DockListRequest.class).getResults();
    }

    private String jsonResponse() {
        return "{\n" +
                "    \"results\": [\n" +
                "        {\n" +
                "            \"address\": \"Magistri\",\n" +
                "            \"area\": {\n" +
                "                \"@class\": \"GeoPolygon\",\n" +
                "                \"points\": [\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.38053545721047,\n" +
                "                        \"longitude\": 26.724455169796784\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.380591705794984,\n" +
                "                        \"longitude\": 26.724739483952362\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.38103606645955,\n" +
                "                        \"longitude\": 26.724417618870575\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.38098544337524,\n" +
                "                        \"longitude\": 26.724101118206818\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.38053545721047,\n" +
                "                        \"longitude\": 26.724455169796784\n" +
                "                    }\n" +
                "                ]\n" +
                "            },\n" +
                "            \"areaCentroid\": {\n" +
                "                \"@class\": \"GeoPoint\",\n" +
                "                \"latitude\": 58.38079076345506,\n" +
                "                \"longitude\": 26.724423279705707\n" +
                "            },\n" +
                "            \"cyclesInStation\": [],\n" +
                "            \"description\": \"Magistri\",\n" +
                "            \"dockingStationType\": \"REGULAR\",\n" +
                "            \"freeDocksCount\": 8,\n" +
                "            \"freeSpacesCount\": 50,\n" +
                "            \"fullCycleStockingCount\": 16,\n" +
                "            \"hasCCReader\": false,\n" +
                "            \"hasKiosk\": false,\n" +
                "            \"id\": \"159b0f05-3d1e-4039-9b09-1f6641c27633\",\n" +
                "            \"lastUpdateCycleCountAt\": 1572727712026,\n" +
                "            \"lockingStationType\": \"PHYSICAL\",\n" +
                "            \"lowCycleStockingCount\": 0,\n" +
                "            \"name\": \"Magistri\",\n" +
                "            \"overFullCycleStockingCount\": 50,\n" +
                "            \"primaryLockedCycleCount\": 8,\n" +
                "            \"public\": true,\n" +
                "            \"secondaryLockedCycleCount\": 0,\n" +
                "            \"serialNumber\": \"56\",\n" +
                "            \"stationStatus\": \"OPEN\",\n" +
                "            \"stationStockingStatus\": \"NORMAL\",\n" +
                "            \"stationType\": \"FIXED\",\n" +
                "            \"totalLockedCycleCount\": 8,\n" +
                "            \"userFavorite\": false\n" +
                "        },\n" +
                "        {\n" +
                "            \"address\": \"S\\u00f5pruse sild\",\n" +
                "            \"area\": {\n" +
                "                \"@class\": \"GeoCircle\",\n" +
                "                \"latitude\": 58.36924551095158,\n" +
                "                \"longitude\": 26.740097496483028,\n" +
                "                \"radius\": 0.0005621162940052585\n" +
                "            },\n" +
                "            \"areaCentroid\": {\n" +
                "                \"@class\": \"GeoPoint\",\n" +
                "                \"latitude\": 58.36924551095159,\n" +
                "                \"longitude\": 26.74009749648303\n" +
                "            },\n" +
                "            \"cyclesInStation\": [],\n" +
                "            \"description\": \"S\\u00f5pruse sild\",\n" +
                "            \"dockingStationType\": \"REGULAR\",\n" +
                "            \"freeDocksCount\": 1,\n" +
                "            \"freeSpacesCount\": 28,\n" +
                "            \"fullCycleStockingCount\": 10,\n" +
                "            \"hasCCReader\": false,\n" +
                "            \"hasKiosk\": false,\n" +
                "            \"id\": \"ceb07a73-2dfa-43b2-a530-205caa8cd520\",\n" +
                "            \"lastUpdateCycleCountAt\": 1572726190417,\n" +
                "            \"lockingStationType\": \"PHYSICAL\",\n" +
                "            \"lowCycleStockingCount\": 0,\n" +
                "            \"name\": \"S\\u00f5pruse sild\",\n" +
                "            \"overFullCycleStockingCount\": 30,\n" +
                "            \"primaryLockedCycleCount\": 9,\n" +
                "            \"public\": true,\n" +
                "            \"secondaryLockedCycleCount\": 2,\n" +
                "            \"serialNumber\": \"34\",\n" +
                "            \"stationStatus\": \"OPEN\",\n" +
                "            \"stationStockingStatus\": \"OVERFULL\",\n" +
                "            \"stationType\": \"FIXED\",\n" +
                "            \"totalLockedCycleCount\": 11,\n" +
                "            \"userFavorite\": false\n" +
                "        },\n" +
                "        {\n" +
                "            \"address\": \"Viie tee rist\",\n" +
                "            \"area\": {\n" +
                "                \"@class\": \"GeoCircle\",\n" +
                "                \"latitude\": 58.355114113189806,\n" +
                "                \"longitude\": 26.709266920776606,\n" +
                "                \"radius\": 0.0004953928806933036\n" +
                "            },\n" +
                "            \"areaCentroid\": {\n" +
                "                \"@class\": \"GeoPoint\",\n" +
                "                \"latitude\": 58.35511411318977,\n" +
                "                \"longitude\": 26.709266920776592\n" +
                "            },\n" +
                "            \"cyclesInStation\": [],\n" +
                "            \"description\": \"Viie tee rist\",\n" +
                "            \"dockingStationType\": \"REGULAR\",\n" +
                "            \"freeDocksCount\": 3,\n" +
                "            \"freeSpacesCount\": 10,\n" +
                "            \"fullCycleStockingCount\": 9,\n" +
                "            \"hasCCReader\": false,\n" +
                "            \"hasKiosk\": false,\n" +
                "            \"id\": \"f2d5ca7f-54ae-40c3-bdf2-4daabe509ccf\",\n" +
                "            \"lastUpdateCycleCountAt\": 1572722831249,\n" +
                "            \"lockingStationType\": \"PHYSICAL\",\n" +
                "            \"lowCycleStockingCount\": 0,\n" +
                "            \"name\": \"Viie tee rist\",\n" +
                "            \"overFullCycleStockingCount\": 10,\n" +
                "            \"primaryLockedCycleCount\": 6,\n" +
                "            \"public\": true,\n" +
                "            \"secondaryLockedCycleCount\": 0,\n" +
                "            \"serialNumber\": \"25\",\n" +
                "            \"stationStatus\": \"OPEN\",\n" +
                "            \"stationStockingStatus\": \"NORMAL\",\n" +
                "            \"stationType\": \"FIXED\",\n" +
                "            \"totalLockedCycleCount\": 6,\n" +
                "            \"userFavorite\": false\n" +
                "        },\n" +
                "        {\n" +
                "            \"address\": \"M\\u00f5isavahe\",\n" +
                "            \"area\": {\n" +
                "                \"@class\": \"GeoCircle\",\n" +
                "                \"latitude\": 58.372497773876134,\n" +
                "                \"longitude\": 26.779650840146243,\n" +
                "                \"radius\": 0.0002576689011810136\n" +
                "            },\n" +
                "            \"areaCentroid\": {\n" +
                "                \"@class\": \"GeoPoint\",\n" +
                "                \"latitude\": 58.37249777387615,\n" +
                "                \"longitude\": 26.77965084014625\n" +
                "            },\n" +
                "            \"cyclesInStation\": [],\n" +
                "            \"description\": \"M\\u00f5isavahe\",\n" +
                "            \"dockingStationType\": \"REGULAR\",\n" +
                "            \"freeDocksCount\": 13,\n" +
                "            \"freeSpacesCount\": 40,\n" +
                "            \"fullCycleStockingCount\": 14,\n" +
                "            \"hasCCReader\": false,\n" +
                "            \"hasKiosk\": false,\n" +
                "            \"id\": \"28875f17-edf2-443c-b914-0f953328ac06\",\n" +
                "            \"lastUpdateCycleCountAt\": 1572728241087,\n" +
                "            \"lockingStationType\": \"PHYSICAL\",\n" +
                "            \"lowCycleStockingCount\": 0,\n" +
                "            \"name\": \"M\\u00f5isavahe\",\n" +
                "            \"overFullCycleStockingCount\": 40,\n" +
                "            \"primaryLockedCycleCount\": 1,\n" +
                "            \"public\": true,\n" +
                "            \"secondaryLockedCycleCount\": 0,\n" +
                "            \"serialNumber\": \"04\",\n" +
                "            \"stationStatus\": \"OPEN\",\n" +
                "            \"stationStockingStatus\": \"NORMAL\",\n" +
                "            \"stationType\": \"FIXED\",\n" +
                "            \"totalLockedCycleCount\": 1,\n" +
                "            \"userFavorite\": false\n" +
                "        }\n" +
                "    ],\n" +
                "    \"total\": 68\n" +
                "}\n";
    }
}