package ee.ut.its.tartusmartbike.dock.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.List;

import ee.ut.its.tartusmartbike.api.DockApi;
import ee.ut.its.tartusmartbike.model.DockListRequest;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

class DockApiTest {

    @Mock
    private OkHttpClient okHttpClient;

    @Mock
    private Call call;

    private DockApi dockApi;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        doReturn(call).when(okHttpClient).newCall(any(Request.class));
        dockApi = new DockApi(okHttpClient);
    }

    @Test
    void allTest() throws Exception {
        //given
        Request mockRequest = mockRequest();
        Response mockResponse = mockResponse(mockRequest);
        doReturn(mockResponse).when(call).execute();

        //when
        List<DockListRequest.DockResponse> allDocks = dockApi.all();

        //then
        assertThat(allDocks).hasSize(4);
    }

    private Request mockRequest() {
        return new Request.Builder()
                .url("https://some-url.com")
                .build();
    }

    private Response mockResponse(Request mockRequest) {
        return new Response.Builder()
                .request(mockRequest)
                .protocol(Protocol.HTTP_2)
                .code(200) // status code
                .message("")
                .body(ResponseBody.create(
                        MediaType.get("application/json; charset=utf-8"),
                        jsonResponse()
                ))
                .build();
    }

    private String jsonResponse() {
        return "{\n" +
                "    \"results\": [\n" +
                "        {\n" +
                "            \"address\": \"Magistri\",\n" +
                "            \"area\": {\n" +
                "                \"@class\": \"GeoPolygon\",\n" +
                "                \"points\": [\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.38053545721047,\n" +
                "                        \"longitude\": 26.724455169796784\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.380591705794984,\n" +
                "                        \"longitude\": 26.724739483952362\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.38103606645955,\n" +
                "                        \"longitude\": 26.724417618870575\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.38098544337524,\n" +
                "                        \"longitude\": 26.724101118206818\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"@class\": \"GeoPoint\",\n" +
                "                        \"latitude\": 58.38053545721047,\n" +
                "                        \"longitude\": 26.724455169796784\n" +
                "                    }\n" +
                "                ]\n" +
                "            },\n" +
                "            \"areaCentroid\": {\n" +
                "                \"@class\": \"GeoPoint\",\n" +
                "                \"latitude\": 58.38079076345506,\n" +
                "                \"longitude\": 26.724423279705707\n" +
                "            },\n" +
                "            \"cyclesInStation\": [],\n" +
                "            \"description\": \"Magistri\",\n" +
                "            \"dockingStationType\": \"REGULAR\",\n" +
                "            \"freeDocksCount\": 8,\n" +
                "            \"freeSpacesCount\": 50,\n" +
                "            \"fullCycleStockingCount\": 16,\n" +
                "            \"hasCCReader\": false,\n" +
                "            \"hasKiosk\": false,\n" +
                "            \"id\": \"159b0f05-3d1e-4039-9b09-1f6641c27633\",\n" +
                "            \"lastUpdateCycleCountAt\": 1572727712026,\n" +
                "            \"lockingStationType\": \"PHYSICAL\",\n" +
                "            \"lowCycleStockingCount\": 0,\n" +
                "            \"name\": \"Magistri\",\n" +
                "            \"overFullCycleStockingCount\": 50,\n" +
                "            \"primaryLockedCycleCount\": 8,\n" +
                "            \"public\": true,\n" +
                "            \"secondaryLockedCycleCount\": 0,\n" +
                "            \"serialNumber\": \"56\",\n" +
                "            \"stationStatus\": \"OPEN\",\n" +
                "            \"stationStockingStatus\": \"NORMAL\",\n" +
                "            \"stationType\": \"FIXED\",\n" +
                "            \"totalLockedCycleCount\": 8,\n" +
                "            \"userFavorite\": false\n" +
                "        },\n" +
                "        {\n" +
                "            \"address\": \"Aleksandri\",\n" +
                "            \"area\": {\n" +
                "                \"@class\": \"GeoCircle\",\n" +
                "                \"latitude\": 58.376155760301096,\n" +
                "                \"longitude\": 26.72882871432966,\n" +
                "                \"radius\": 0.0003433327633552717\n" +
                "            },\n" +
                "            \"areaCentroid\": {\n" +
                "                \"@class\": \"GeoPoint\",\n" +
                "                \"latitude\": 58.37615576030109,\n" +
                "                \"longitude\": 26.728828714329662\n" +
                "            },\n" +
                "            \"cyclesInStation\": [],\n" +
                "            \"description\": \"Aleksandri\",\n" +
                "            \"dockingStationType\": \"REGULAR\",\n" +
                "            \"freeDocksCount\": 5,\n" +
                "            \"freeSpacesCount\": 20,\n" +
                "            \"fullCycleStockingCount\": 6,\n" +
                "            \"hasCCReader\": false,\n" +
                "            \"hasKiosk\": false,\n" +
                "            \"id\": \"953f818c-dbc5-48f6-93e2-7ef0dcad8d67\",\n" +
                "            \"lastUpdateCycleCountAt\": 1572725154090,\n" +
                "            \"lockingStationType\": \"PHYSICAL\",\n" +
                "            \"lowCycleStockingCount\": 0,\n" +
                "            \"name\": \"Aleksandri\",\n" +
                "            \"overFullCycleStockingCount\": 20,\n" +
                "            \"primaryLockedCycleCount\": 1,\n" +
                "            \"public\": true,\n" +
                "            \"secondaryLockedCycleCount\": 0,\n" +
                "            \"serialNumber\": \"32\",\n" +
                "            \"stationStatus\": \"OPEN\",\n" +
                "            \"stationStockingStatus\": \"NORMAL\",\n" +
                "            \"stationType\": \"FIXED\",\n" +
                "            \"totalLockedCycleCount\": 1,\n" +
                "            \"userFavorite\": false\n" +
                "        },\n" +
                "        {\n" +
                "            \"address\": \"Eeden\",\n" +
                "            \"area\": {\n" +
                "                \"@class\": \"GeoCircle\",\n" +
                "                \"latitude\": 58.37337251322241,\n" +
                "                \"longitude\": 26.751663997516516,\n" +
                "                \"radius\": 0.0003608394897129412\n" +
                "            },\n" +
                "            \"areaCentroid\": {\n" +
                "                \"@class\": \"GeoPoint\",\n" +
                "                \"latitude\": 58.3733725132224,\n" +
                "                \"longitude\": 26.75166399751651\n" +
                "            },\n" +
                "            \"cyclesInStation\": [],\n" +
                "            \"description\": \"Eeden\",\n" +
                "            \"dockingStationType\": \"REGULAR\",\n" +
                "            \"freeDocksCount\": 6,\n" +
                "            \"freeSpacesCount\": 38,\n" +
                "            \"fullCycleStockingCount\": 12,\n" +
                "            \"hasCCReader\": false,\n" +
                "            \"hasKiosk\": false,\n" +
                "            \"id\": \"952f8592-43f8-43b5-ab66-6a5f8a643b53\",\n" +
                "            \"lastUpdateCycleCountAt\": 1572714874947,\n" +
                "            \"lockingStationType\": \"PHYSICAL\",\n" +
                "            \"lowCycleStockingCount\": 0,\n" +
                "            \"name\": \"Eeden\",\n" +
                "            \"overFullCycleStockingCount\": 40,\n" +
                "            \"primaryLockedCycleCount\": 6,\n" +
                "            \"public\": true,\n" +
                "            \"secondaryLockedCycleCount\": 2,\n" +
                "            \"serialNumber\": \"68\",\n" +
                "            \"stationStatus\": \"OPEN\",\n" +
                "            \"stationStockingStatus\": \"NORMAL\",\n" +
                "            \"stationType\": \"FIXED\",\n" +
                "            \"totalLockedCycleCount\": 8,\n" +
                "            \"userFavorite\": false\n" +
                "        },\n" +
                "        {\n" +
                "            \"address\": \"M\\u00f5isavahe\",\n" +
                "            \"area\": {\n" +
                "                \"@class\": \"GeoCircle\",\n" +
                "                \"latitude\": 58.372497773876134,\n" +
                "                \"longitude\": 26.779650840146243,\n" +
                "                \"radius\": 0.0002576689011810136\n" +
                "            },\n" +
                "            \"areaCentroid\": {\n" +
                "                \"@class\": \"GeoPoint\",\n" +
                "                \"latitude\": 58.37249777387615,\n" +
                "                \"longitude\": 26.77965084014625\n" +
                "            },\n" +
                "            \"cyclesInStation\": [],\n" +
                "            \"description\": \"M\\u00f5isavahe\",\n" +
                "            \"dockingStationType\": \"REGULAR\",\n" +
                "            \"freeDocksCount\": 13,\n" +
                "            \"freeSpacesCount\": 40,\n" +
                "            \"fullCycleStockingCount\": 14,\n" +
                "            \"hasCCReader\": false,\n" +
                "            \"hasKiosk\": false,\n" +
                "            \"id\": \"28875f17-edf2-443c-b914-0f953328ac06\",\n" +
                "            \"lastUpdateCycleCountAt\": 1572728241087,\n" +
                "            \"lockingStationType\": \"PHYSICAL\",\n" +
                "            \"lowCycleStockingCount\": 0,\n" +
                "            \"name\": \"M\\u00f5isavahe\",\n" +
                "            \"overFullCycleStockingCount\": 40,\n" +
                "            \"primaryLockedCycleCount\": 1,\n" +
                "            \"public\": true,\n" +
                "            \"secondaryLockedCycleCount\": 0,\n" +
                "            \"serialNumber\": \"04\",\n" +
                "            \"stationStatus\": \"OPEN\",\n" +
                "            \"stationStockingStatus\": \"NORMAL\",\n" +
                "            \"stationType\": \"FIXED\",\n" +
                "            \"totalLockedCycleCount\": 1,\n" +
                "            \"userFavorite\": false\n" +
                "        }\n" +
                "    ],\n" +
                "    \"total\": 68\n" +
                "}\n";
    }
}