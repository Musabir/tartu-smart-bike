package ee.ut.its.tartusmartbike.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ee.ut.its.tartusmartbike.R;
import ee.ut.its.tartusmartbike.activity.NavigationActivity;
import ee.ut.its.tartusmartbike.model.RouteListModel;

public class RouteListAdapter extends BaseAdapter {
    private Context context;
    private List<RouteListModel> routes;

    public RouteListAdapter(Context context, List<RouteListModel> routes) {
        this.context = context;
        this.routes = routes;
    }

    @Override
    public int getCount() {
        return routes.size();
    }

    @Override
    public RouteListModel getItem(int position) {
        return routes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_route, parent, false);
        }
        TextView sourceMin = view.findViewById(R.id.source_walk_min);
        TextView destMin = view.findViewById(R.id.dest_walk_min);
        TextView bikeMin = view.findViewById(R.id.bike_min);
        TextView sourceDist = view.findViewById(R.id.source_walk_dist);
        TextView destDist = view.findViewById(R.id.dest_walk_dist);
        TextView bikeDist = view.findViewById(R.id.bike_dist);
        TextView distance = view.findViewById(R.id.distance);
        TextView duration = view.findViewById(R.id.duration);
        TextView recommendation = view.findViewById(R.id.recommendation);
        ImageView electricBikeIcon = view.findViewById(R.id.electric_bike);
        final RouteListModel current = getItem(position);
        sourceMin.setText(convertSecondToMin(current.getSourceToDock().duration()) + " min");
        destMin.setText(convertSecondToMin(current.getDestDockToDest().duration()) + " min");
        bikeMin.setText(convertSecondToMin(current.getSourceDockToDestDock().duration()) + " min");
        sourceDist.setText(convertMeterToKilometer(current.getSourceToDock().distance()) + " km");
        destDist.setText(convertMeterToKilometer(current.getDestDockToDest().distance()) + " km");
        bikeDist.setText(convertMeterToKilometer(current.getSourceDockToDestDock().distance()) + " km");
        distance.setText(calculateDistance(current) + " km");
        duration.setText(calculateDuration(current) + " min");

        if (current.getNumOfElectricBike() != null && current.getNumOfElectricBike() > 0)
            electricBikeIcon.setVisibility(View.VISIBLE);
        else electricBikeIcon.setVisibility(View.GONE);

        if (current.getAltitudeDiff() > 20)
            recommendation.setVisibility(View.VISIBLE);
        else
            recommendation.setVisibility(View.GONE);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NavigationActivity.class);
                intent.putExtra("currentElement", current);
                context.startActivity(intent);
            }
        });
        return view;
    }

    private double calculateDistance(RouteListModel routeListModel) {
        return convertMeterToKilometer(routeListModel.getSourceToDock().distance()
                + routeListModel.getDestDockToDest().distance()
                + routeListModel.getSourceDockToDestDock().distance());
    }

    private double convertMeterToKilometer(double totalDistance) {
        double ff = totalDistance / 1000.0;
        BigDecimal bd = BigDecimal.valueOf(ff);
        bd = bd.setScale(1, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private double convertSecondToMin(double totalSeconds) {
        return TimeUnit.SECONDS.toMinutes((long) totalSeconds);
    }

    private double calculateDuration(RouteListModel routeListModel) {
        return convertSecondToMin(routeListModel.getSourceToDock().duration()
                + routeListModel.getDestDockToDest().duration()
                + routeListModel.getSourceDockToDestDock().duration());
    }
}
