package ee.ut.its.tartusmartbike.api;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import ee.ut.its.tartusmartbike.BuildConfig;
import ee.ut.its.tartusmartbike.model.DockListRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DockApi {

    public DockApi(OkHttpClient client) {
        this.client = client;
    }

    private final OkHttpClient client;
    private final Gson gson = new Gson();
    private final Request request = new Request.Builder()
            .url(BuildConfig.API_URL)
            .method("POST", RequestBody.create("{}", MediaType.get("application/json; charset=utf-8")))
            .header("Content-Type", "application/json")
            .build();


    public List<DockListRequest.DockResponse> all() throws IOException {
        Response response = client.newCall(request).execute();
        String apiResponse = response.body().string();
        DockListRequest dockListRequest = this.gson.fromJson(apiResponse, DockListRequest.class);
        return dockListRequest.getResults();
    }
}
