package ee.ut.its.tartusmartbike.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DockListRequest {

    private Integer total;

    private List<DockResponse> results;

    public List<DockResponse> getResults() {
        return results;
    }

    public static class DockResponse {
        private String stationType;

        private String description;

        private String hasCCReader;

        private String primaryLockedCycleCount;

        private PointResponse areaCentroid;

        private String dockingStationType;

        private String fullCycleStockingCount;

        private String stationStatus;

        private String overFullCycleStockingCount;

        private String id;

        private String totalLockedCycleCount;

        private String lastUpdateCycleCountAt;

        private String secondaryLockedCycleCount;

        private String address;

        private String serialNumber;

        private String freeSpacesCount;

        private String[] cyclesInStation;

        private String stationStockingStatus;

        private String lockingStationType;

        private String name;

        private String freeDocksCount;

        private String lowCycleStockingCount;

        public String getAddress() {
            return address;
        }

        public String getName() {
            return name;
        }

        public PointResponse getAreaCentroid() {
            return areaCentroid;
        }

        public String getStationType() {
            return stationType;
        }

        public String getDescription() {
            return description;
        }

        public String getHasCCReader() {
            return hasCCReader;
        }

        public String getPrimaryLockedCycleCount() {
            return primaryLockedCycleCount;
        }

        public String getDockingStationType() {
            return dockingStationType;
        }

        public String getFullCycleStockingCount() {
            return fullCycleStockingCount;
        }

        public String getStationStatus() {
            return stationStatus;
        }

        public String getOverFullCycleStockingCount() {
            return overFullCycleStockingCount;
        }

        public String getId() {
            return id;
        }

        public String getTotalLockedCycleCount() {
            return totalLockedCycleCount;
        }

        public String getLastUpdateCycleCountAt() {
            return lastUpdateCycleCountAt;
        }

        public String getSecondaryLockedCycleCount() {
            return secondaryLockedCycleCount;
        }

        public String getSerialNumber() {
            return serialNumber;
        }

        public String getFreeSpacesCount() {
            return freeSpacesCount;
        }

        public String[] getCyclesInStation() {
            return cyclesInStation;
        }

        public String getStationStockingStatus() {
            return stationStockingStatus;
        }

        public String getLockingStationType() {
            return lockingStationType;
        }

        public String getFreeDocksCount() {
            return freeDocksCount;
        }

        public String getLowCycleStockingCount() {
            return lowCycleStockingCount;
        }

        @Override
        public String toString() {
            return "DockResponse{" +
                    "stationType='" + stationType + '\'' +
                    ", description='" + description + '\'' +
                    ", hasCCReader='" + hasCCReader + '\'' +
                    ", primaryLockedCycleCount='" + primaryLockedCycleCount + '\'' +
                    ", areaCentroid=" + areaCentroid +
                    ", dockingStationType='" + dockingStationType + '\'' +
                    ", fullCycleStockingCount='" + fullCycleStockingCount + '\'' +
                    ", stationStatus='" + stationStatus + '\'' +
                    ", overFullCycleStockingCount='" + overFullCycleStockingCount + '\'' +
                    ", id='" + id + '\'' +
                    ", totalLockedCycleCount='" + totalLockedCycleCount + '\'' +
                    ", lastUpdateCycleCountAt='" + lastUpdateCycleCountAt + '\'' +
                    ", secondaryLockedCycleCount='" + secondaryLockedCycleCount + '\'' +
                    ", address='" + address + '\'' +
                    ", serialNumber='" + serialNumber + '\'' +
                    ", freeSpacesCount='" + freeSpacesCount + '\'' +
                    ", cyclesInStation=" + Arrays.toString(cyclesInStation) +
                    ", stationStockingStatus='" + stationStockingStatus + '\'' +
                    ", lockingStationType='" + lockingStationType + '\'' +
                    ", name='" + name + '\'' +
                    ", freeDocksCount='" + freeDocksCount + '\'' +
                    ", lowCycleStockingCount='" + lowCycleStockingCount + '\'' +
                    '}';
        }
    }

    public static class AreaResponse {
        private ArrayList<PointResponse> points;
    }

    public static class PointResponse {
        private Double latitude;

        private Double longitude;

        public Double getLatitude() {
            return latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        @Override
        public String toString() {
            return "PointResponse{" +
                    "latitude=" + latitude +
                    ", longitude=" + longitude +
                    '}';
        }
    }

    public static class CircleResponse {

        //private PointResponse pointResponse;

        private Double latitude;

        private Double longitude;

        private Double radius;

        @Override
        public String toString() {
            return "CircleResponse{" +
                    "latitude=" + latitude +
                    ", longitude=" + longitude +
                    ", radius=" + radius +
                    '}';
        }
    }
}
