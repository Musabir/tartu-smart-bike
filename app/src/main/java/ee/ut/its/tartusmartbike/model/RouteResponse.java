package ee.ut.its.tartusmartbike.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteResponse {


    private PointResponse src;
    private PointResponse dest;

    public RouteResponse(PointResponse src, PointResponse dest) {
        this.src = src;
        this.dest = dest;
    }

    public RouteResponse() {

    }

    public PointResponse getSrc() {
        return src;
    }

    public void setSrc(PointResponse src) {
        this.src = src;
    }

    public PointResponse getDest() {
        return dest;
    }

    public void setDest(PointResponse dest) {
        this.dest = dest;
    }


    @Override
    public String toString() {
        return "RouteResponse{" +
                "src=" + src +
                ", dest=" + dest +
                '}';
    }
}
