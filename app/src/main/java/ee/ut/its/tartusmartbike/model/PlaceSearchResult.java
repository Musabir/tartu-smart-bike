package ee.ut.its.tartusmartbike.model;

public class PlaceSearchResult {
    private String label;
    private String language;
    private String locationId;

    public PlaceSearchResult(){

    }

    public PlaceSearchResult(String label, String language, String locationId) {
        this.label = label;
        this.language = language;
        this.locationId = locationId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @Override
    public String toString() {
        return "PlaceSearchResult{" +
                "label='" + label + '\'' +
                ", language='" + language + '\'' +
                ", locationId='" + locationId + '\'' +
                '}';
    }
}
