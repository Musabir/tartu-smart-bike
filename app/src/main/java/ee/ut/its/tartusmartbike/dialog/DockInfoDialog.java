package ee.ut.its.tartusmartbike.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import ee.ut.its.tartusmartbike.R;
import ee.ut.its.tartusmartbike.model.DockListRequest;

public class DockInfoDialog extends Dialog {

    private DockListRequest.DockResponse dock;
    private TextView electricBikesCount, manualBikesCount, emptySlotsCount, address;

    public DockInfoDialog(@NonNull Context context, DockListRequest.DockResponse dock) {
        super(context);
        this.dock = dock;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_dock_info);
        electricBikesCount = findViewById(R.id.smart_bike_count);
        manualBikesCount = findViewById(R.id.regular_bike_count);
        emptySlotsCount = findViewById(R.id.empty_dock_count);
        address = findViewById(R.id.address_txt);

        setValues();
        RelativeLayout cancelButton = findViewById(R.id.cancel_txt_lyt);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void setValues() {
        electricBikesCount.setText(dock.getSecondaryLockedCycleCount());
        manualBikesCount.setText(dock.getPrimaryLockedCycleCount());
        emptySlotsCount.setText(dock.getFreeDocksCount());
        address.setText("Address: "+ dock.getAddress());
    }

}
