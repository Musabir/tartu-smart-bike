package ee.ut.its.tartusmartbike.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import ee.ut.its.tartusmartbike.R;
import ee.ut.its.tartusmartbike.api.DockApi;
import ee.ut.its.tartusmartbike.common.Helper;
import ee.ut.its.tartusmartbike.dialog.DockInfoDialog;
import ee.ut.its.tartusmartbike.model.DockListRequest;
import ee.ut.its.tartusmartbike.services.DockService;
import okhttp3.OkHttpClient;

import static com.mapbox.mapboxsdk.maps.Style.MAPBOX_STREETS;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, MapboxMap.OnMapClickListener,
        PermissionsListener {
    // variables for adding location layer
    private MapView mapView;
    private MapboxMap mapboxMap;
    private RelativeLayout searchBtn;
    // variables for adding location layer
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;
    // variables for calculating and drawing a route
    private DirectionsRoute currentRoute;
    private List<DirectionsRoute> routes;
    private static final String TAG = "DirectionsActivity";
    private NavigationMapRoute navigationMapRoute;
    // variables needed to initialize navigation
    private Button button;
    private ArrayList<String> layerIds;
    private List<DockListRequest.DockResponse> dockListRequests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_main);
        mapView = findViewById(R.id.mapView);
        searchBtn = findViewById(R.id.search_btn);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        routes = new ArrayList<>();
        layerIds = new ArrayList<>();
        dockListRequests = new ArrayList<>();

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(MAPBOX_STREETS, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                centralizeMap(style);
                enableLocationComponent(style);
                mapboxMap.addOnMapClickListener(MainActivity.this);

            }
        });
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }

    }

    private void addDestinationIconSymbolLayer(List<DockListRequest.DockResponse> responses, @NonNull Style loadedMapStyle) {
        layerIds = new ArrayList<String>();
        for (DockListRequest.DockResponse dock : responses) {
            LayoutInflater li = LayoutInflater.from(this);
            View customCluster = li.inflate(R.layout.custom_cluster, null);
            TextView count = customCluster.findViewById(R.id.count);
            count.setText(dock.getTotalLockedCycleCount());
            loadedMapStyle.addImage(dock.getId(), Helper.createBitmapFromLayout(customCluster));
            GeoJsonSource geoJsonSource = new GeoJsonSource(dock.getId());
            Point secondDestination = Point.fromLngLat(dock.getAreaCentroid().getLongitude(), dock.getAreaCentroid().getLatitude());
            Feature feature = Feature.fromGeometry(secondDestination);
            feature.addStringProperty("id", dock.getId());
            geoJsonSource.setGeoJson(feature);
            loadedMapStyle.addSource(geoJsonSource);
            SymbolLayer dockSymbol = new SymbolLayer(dock.getId(), dock.getId());
            dockSymbol.withProperties(
                    iconImage(dock.getId()),
                    iconAllowOverlap(true),
                    iconIgnorePlacement(true)
            );
            loadedMapStyle.addLayer(dockSymbol);
            layerIds.add(dock.getId());
        }
    }

    private void centralizeMap(Style style) {

        try {
            dockListRequests = new DockService(new DockApi(new OkHttpClient())).execute().get();
            addDestinationIconSymbolLayer(dockListRequests, style);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    private void showDockInfoDialog(String id) {
        DockListRequest.DockResponse dock = getDockById(id);
        if (dock != null) {
            Dialog dialog = new DockInfoDialog(this, dock);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimations_PauseDialogAnimation;
            dialog.show();
        } else
            Toast.makeText(this, "Something goes wrong", Toast.LENGTH_SHORT).show();
    }

    private DockListRequest.DockResponse getDockById(String id) {

        for (DockListRequest.DockResponse dock : dockListRequests)
            if (dock.getId().equals(id))
                return dock;
        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        final PointF pixel = mapboxMap.getProjection().toScreenLocation(point);
        String[] arr = new String[layerIds.size()];
        arr = layerIds.toArray(arr);
        List<Feature> features = mapboxMap.queryRenderedFeatures(pixel, arr);
        if (!features.isEmpty()) {
            showDockInfoDialog(features.get(0).properties().getAsJsonPrimitive("id").getAsString());
        }

        return true;
    }
}

