package ee.ut.its.tartusmartbike.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.gson.JsonObject;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.OnSymbolDragListener;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;

import java.util.List;

import ee.ut.its.tartusmartbike.R;
import ee.ut.its.tartusmartbike.model.SearchFieldResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity implements OnMapReadyCallback, PermissionsListener {

    private static final int REQUEST_CODE_AUTOCOMPLETE_ORIGIN = 1;
    private static final int REQUEST_CODE_AUTOCOMPLETE_DESTINATION = 2;
    private MapView mapView;
    private RelativeLayout searchRoute;
    private MapboxMap mapboxMap;
    private CarmenFeature university;
    private CarmenFeature dormitory;
    private String originGeojsonSourceLayerId = "originGeojsonSourceLayerId";
    private String destinationGeojsonSourceLayerId = "destinationGeojsonSourceLayerId";
    private String originSymbolIconId = "originSymbolIconId";
    private String destinationSymbolIconId = "destinationSymbolIconId";
    private TextView originAddress, destinationAddress;
    private SymbolManager symbolManager;
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;
    private Symbol originSymbol, destinationSymbol;
    private SearchFieldResult originAddressResult, destinationAddressResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));

        setContentView(R.layout.activity_search);

        mapView = findViewById(R.id.mapView);
        originAddress = findViewById(R.id.origin_address);
        destinationAddress = findViewById(R.id.destination_address);
        searchRoute = findViewById(R.id.search_route);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        originAddressResult = new SearchFieldResult();
        destinationAddressResult = new SearchFieldResult();
        searchRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (originAddress.getText().length() > 0 && destinationAddress.getText().length() > 0) {
                    originAddressResult.setAddress(originAddress.getText().toString());
                    originAddressResult.setPoint(originSymbol.getGeometry());
                    destinationAddressResult.setAddress(destinationAddress.getText().toString());
                    destinationAddressResult.setPoint(destinationSymbol.getGeometry());

                    Intent intent = new Intent(SearchActivity.this, RoutesListActivity.class);
                    intent.putExtra("originAddress", originAddressResult);
                    intent.putExtra("destinationAddress", destinationAddressResult);
                    startActivity(intent);
                } else
                    Toast.makeText(SearchActivity.this, "Please, enter origin and destination addresses", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                symbolManager = new SymbolManager(mapView, mapboxMap, style);
                initSearchFields();
                addUserLocations();
                style.addImage(originSymbolIconId, ContextCompat.getDrawable(SearchActivity.this, R.drawable.ic_marker_origin));
                style.addImage(destinationSymbolIconId, ContextCompat.getDrawable(SearchActivity.this, R.drawable.ic_marker_destination));
                setupLayer();
                setMarkerDragListener();
                enableLocationComponent(style);

            }
        });
    }

    private void setMarkerDragListener() {
        symbolManager.addDragListener(new OnSymbolDragListener() {
            @Override
            public void onAnnotationDragStarted(Symbol annotation) {
            }

            @Override
            public void onAnnotationDrag(Symbol annotation) {

            }

            @Override
            public void onAnnotationDragFinished(Symbol annotation) {
                if (annotation.getIconImage().equals(originSymbolIconId)) {
                    getLocationAddressFromLatLng(annotation.getGeometry(), originSymbolIconId);
                }
                if (annotation.getIconImage().equals(destinationSymbolIconId)) {
                    getLocationAddressFromLatLng(annotation.getGeometry(), destinationSymbolIconId);
                }
            }
        });
    }


    private void initSearchFields() {
        originAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken(Mapbox.getAccessToken() != null ? Mapbox.getAccessToken() : getString(R.string.access_token))
                        .placeOptions(PlaceOptions.builder()
                                .backgroundColor(Color.parseColor("#EEEEEE"))
                                .limit(10)
                                .addInjectedFeature(university)
                                .addInjectedFeature(dormitory)
                                .country("EE")
                                .language("et-EE")
                                .build(PlaceOptions.MODE_CARDS))
                        .build(SearchActivity.this);
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE_ORIGIN);
            }
        });

        destinationAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken(Mapbox.getAccessToken() != null ? Mapbox.getAccessToken() : getString(R.string.access_token))
                        .placeOptions(PlaceOptions.builder()
                                .backgroundColor(Color.parseColor("#EEEEEE"))
                                .limit(10)
                                .addInjectedFeature(university)
                                .addInjectedFeature(dormitory)
                                .country("EE")
                                .language("et-EE")
                                .build(PlaceOptions.MODE_CARDS))
                        .build(SearchActivity.this);
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE_DESTINATION);
            }
        });


    }

    private void addUserLocations() {
        university = CarmenFeature.builder().text("University of Tartu")
                .geometry(Point.fromLngLat(26.7176772, 58.3810871))
                .placeName("Ülikooli 18, Tartu")
                .id("mapbox-sf")
                .properties(new JsonObject())
                .build();

        dormitory = CarmenFeature.builder().text("Narva mnt 27 Residence Hall")
                .placeName("Narva maantee 27, Tartu")
                .geometry(Point.fromLngLat(26.7262226, 58.3827486))
                .id("mapbox-dc")
                .properties(new JsonObject())
                .build();
    }

    private void setupLayer() {

        originSymbol = symbolManager.create(new SymbolOptions()
                .withLatLng(new LatLng(58.378025, 26.728493))
                .withIconImage(originSymbolIconId)
                .withIconSize(1.3f)
                .withIconOffset(new Float[]{0f, -1.5f})
                .withTextHaloColor("rgb(181, 55, 55)")
                .withTextHaloWidth(1.0f)
                .withTextSize(10f)
                .withTextColor("rgb(255, 255, 255)")
                .withTextAnchor("top")
                .withTextField("Origin")
                .withDraggable(true)
        );

        destinationSymbol = symbolManager.create(new SymbolOptions()
                .withLatLng(new LatLng(58.3810871, 26.7176772))
                .withIconImage(destinationSymbolIconId)
                .withIconSize(1.3f)
                .withIconOffset(new Float[]{0f, -1.5f})
                .withTextHaloColor("rgb(50,205,50)")
                .withTextHaloWidth(1.0f)
                .withTextSize(10f)
                .withTextColor("rgb(255, 255, 255)")
                .withTextAnchor("top")
                .withTextHaloWidth(1.0f)
                .withTextField("Destination")
                .withDraggable(true)
        );

    }

    private void getLocationAddressFromLatLng(Point point, final String icon) {
        MapboxGeocoding reverseGeocode = MapboxGeocoding.builder()
                .accessToken(getString(R.string.access_token))
                .query(point)
                .geocodingTypes(GeocodingCriteria.TYPE_ADDRESS)
                .build();
        reverseGeocode.enqueueCall(new Callback<GeocodingResponse>() {
            @Override
            public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {
                List<CarmenFeature> results = response.body().features();

                if (results.size() > 0) {
                    if (icon.equals(originSymbolIconId))
                        originAddress.setText(results.get(0).placeName());
                    else
                        destinationAddress.setText(results.get(0).placeName());
                } else {
                    if (icon.equals(originSymbolIconId))
                        originAddress.setText("Pin on map");
                    else
                        destinationAddress.setText("Pin on map");
                }
            }

            @Override
            public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);

            if (mapboxMap != null) {
                Style style = mapboxMap.getStyle();
                if (style != null && requestCode == REQUEST_CODE_AUTOCOMPLETE_ORIGIN) {
                    originSymbol.setLatLng(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                            ((Point) selectedCarmenFeature.geometry()).longitude()));
                    symbolManager.update(originSymbol);

                    mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                            ((Point) selectedCarmenFeature.geometry()).longitude()))
                                    .zoom(14)
                                    .build()), 3000);

                    originAddress.setText(selectedCarmenFeature.placeName());
                } else if (style != null && requestCode == REQUEST_CODE_AUTOCOMPLETE_DESTINATION) {
                    destinationSymbol.setLatLng(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                            ((Point) selectedCarmenFeature.geometry()).longitude()));
                    symbolManager.update(originSymbol);

                    mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                            ((Point) selectedCarmenFeature.geometry()).longitude()))
                                    .zoom(14)
                                    .build()), 3000);

                    destinationAddress.setText(selectedCarmenFeature.placeName());

                }
            }
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            if (locationComponent.getLastKnownLocation() != null)
                originSymbol.setLatLng(new LatLng(locationComponent.getLastKnownLocation().getLatitude(),
                        (locationComponent.getLastKnownLocation().getLongitude())));
            else
                originSymbol.setLatLng(new LatLng(58.3827486,
                        26.7262226));
            symbolManager.update(originSymbol);


        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


}