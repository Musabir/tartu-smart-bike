package ee.ut.its.tartusmartbike.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.suke.widget.SwitchButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import ee.ut.its.tartusmartbike.BuildConfig;
import ee.ut.its.tartusmartbike.R;
import ee.ut.its.tartusmartbike.adapter.RouteListAdapter;
import ee.ut.its.tartusmartbike.model.RouteListModel;
import ee.ut.its.tartusmartbike.model.RouteResponse;
import ee.ut.its.tartusmartbike.model.SearchFieldResult;
import ee.ut.its.tartusmartbike.services.JsonArrayRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutesListActivity extends AppCompatActivity {

    private DirectionsRoute currentRoute;
    private List<RouteListModel> routes;
    private List<RouteListModel> allBikeTypesRoutes;
    private List<RouteListModel> electricBikeRoutes;
    private static final String TAG = "RoutesListActivity";
    private ListView listView;
    private SwitchButton switchButton;
    private TextView originAddress, destinationAddress;
    private RouteListAdapter routeListAdapter;
    private final int MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS = 3;
    private int MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS_SMART_BIKE = 3;
    private int requestCount = 0;
    private SearchFieldResult originAddressResult, destinationAddressResult;
    private ObjectMapper mapper;
    private List<RouteResponse> routeResponses;
    private AlertDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_routes_list);
        originAddress = findViewById(R.id.origin_address);
        destinationAddress = findViewById(R.id.destination_address);
        listView = findViewById(R.id.route_list);
        switchButton = findViewById(R.id.switch_button);
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        routeResponses = new ArrayList<>();
        routes = new ArrayList<>();
        allBikeTypesRoutes = new ArrayList<>();
        electricBikeRoutes = new ArrayList<>();
        routeListAdapter = new RouteListAdapter(RoutesListActivity.this, routes);
        listView.setAdapter(routeListAdapter);
        getOriginAndDestinationAddress();
        findClosestDocks();
        dialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Searching for all closest bikes")
                .setCancelable(true)
                .build();
        dialog.show();

        switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(isChecked && MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS_SMART_BIKE ==0){
                    Toast.makeText(RoutesListActivity.this, "No any electrical bike found", Toast.LENGTH_SHORT).show();
                    routes.clear();
                    routeListAdapter.notifyDataSetChanged();
                }
                else
                findBestRoutes(isChecked);

            }
        });

    }

    private void getOriginAndDestinationAddress() {
        Intent intent = getIntent();
        originAddressResult = (SearchFieldResult) intent.getSerializableExtra("originAddress");
        destinationAddressResult = (SearchFieldResult) intent.getSerializableExtra("destinationAddress");
        originAddress.setText(originAddressResult.getAddress());
        destinationAddress.setText(destinationAddressResult.getAddress());
    }


    private void getRoute(Point origin, Point destination, String profile, final int flag, final int index,
                          final boolean isChecked, final Long numElectricBike, final Long altitudeDiff) {
        NavigationRoute.Builder builder = NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .profile(profile);
        builder.build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        requestCount++;
                        Log.d(TAG, "Response code: " + response.code());
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);

                        if (flag == 0) {
                            routes.get(index).setSourceToDock(currentRoute);
                        } else if (flag == 1) {
                            routes.get(index).setDestDockToDest(currentRoute);
                        } else if (flag == 2) {
                            routes.get(index).setSourceDockToDestDock(currentRoute);
                            routes.get(index).setNumOfElectricBike(numElectricBike);
                            routes.get(index).setAltitudeDiff(altitudeDiff);

                        }
                        if ((!isChecked && requestCount == MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS * 3)
                                || isChecked && requestCount == MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS_SMART_BIKE * 3) {
                            if (!isChecked) {
                                allBikeTypesRoutes.clear();
                                allBikeTypesRoutes.addAll(routes);
                            } else {
                                electricBikeRoutes.clear();
                                electricBikeRoutes.addAll(routes);
                            }
                            routeListAdapter.notifyDataSetChanged();
                            dialog.cancel();
                        }
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        Log.e(TAG, "Error: " + throwable.getMessage());
                        dialog.cancel();

                    }
                });
    }


    private void findClosestDocks() {
        JSONObject body = getRequestBody();

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest postRequest = new JsonArrayRequest(Request.Method.POST,
                BuildConfig.ROUTES_URL, body,
                new com.android.volley.Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // response

                        if (response != null) {
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    routeResponses.add(mapper.readValue(response.getJSONObject(i).toString(), RouteResponse.class));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            findMaxNumberOfElectricBikeDocs();
                            findBestRoutes(false);
                        }

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                        dialog.cancel();

                    }
                }
        );
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);
    }

    private void findBestRoutes(boolean flag) {

        if (flag && electricBikeRoutes.size() > 0) {
            routes.clear();
            routes.addAll(electricBikeRoutes);
            routeListAdapter.notifyDataSetChanged();
        } else if (!flag && allBikeTypesRoutes.size() > 0) {

            routes.clear();
            routes.addAll(allBikeTypesRoutes);
            routeListAdapter.notifyDataSetChanged();
        } else {
            routes.clear();

            if (!flag)
                for (int i = 0; i < MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS; i++)
                    routes.add(new RouteListModel());

            else
                for (int i = 0; i < MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS_SMART_BIKE; i++)
                    routes.add(new RouteListModel());
            if (flag)
                dialog.setMessage("Searching for closest smart bikes");
            else
                dialog.setMessage("Searching for all closest bikes");

            if (!dialog.isShowing())
                dialog.show();
            int counter = 0;
            requestCount = 0;
            for (int i = 0; i < routeResponses.size(); i++) {
                RouteResponse routeResponse = routeResponses.get(i);
                if (!flag || routeResponse.getSrc().getElectricBikes() > 0) {

                    getRoute(originAddressResult.getPoint(),
                            Point.fromLngLat(routeResponse.getSrc().getLongitude(), routeResponse.getSrc().getLatitude()),
                            "walking", 0, counter, flag, null, null);

                    getRoute(Point.fromLngLat(routeResponse.getSrc().getLongitude(), routeResponse.getSrc().getLatitude()),
                            Point.fromLngLat(routeResponse.getDest().getLongitude(), routeResponse.getDest().getLatitude()),
                            "cycling", 2, counter, flag, routeResponse.getSrc().getElectricBikes(),
                            findAltitudeDiff(routeResponse.getSrc().getAltitude(), routeResponse.getDest().getAltitude()));

                    getRoute(Point.fromLngLat(routeResponse.getDest().getLongitude(), routeResponse.getDest().getLatitude()),
                            destinationAddressResult.getPoint(), "walking", 1, counter, flag, null, null);
                    counter++;

                    if (counter == 3)
                        break;
                }
            }
        }
    }

    private Long findAltitudeDiff(String al1, String al2) {
        Long altitude1, altitude2;
        try {
            altitude1 = Long.parseLong(al1);
            altitude2 = Long.parseLong(al2);
            return altitude2 - altitude1;
        } catch (Exception e) {
            return 0L;
        }


    }

    private void findMaxNumberOfElectricBikeDocs() {
        int count = 0;
        for (int i = 0; i < routeResponses.size(); i++) {
            if (routeResponses.get(i).getSrc().getElectricBikes() > 0)
                count++;
        }
        if (count > 3)
            MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS_SMART_BIKE = 3;
        else MAX_NUMBER_OF_ROUTE_LIST_ELEMENTS_SMART_BIKE = count;
    }

    private JSONObject getRequestBody() {
        JSONObject jsonObject1 = new JSONObject();
        JSONObject jsonObject2 = new JSONObject();
        JSONObject jsonObject3 = new JSONObject();
        try {
            jsonObject1.put("lat", originAddressResult.getPoint().latitude());
            jsonObject1.put("lng", originAddressResult.getPoint().longitude());
            jsonObject2.put("lat", destinationAddressResult.getPoint().latitude());
            jsonObject2.put("lng", destinationAddressResult.getPoint().longitude());
            jsonObject3.put("src", jsonObject1);
            jsonObject3.put("dest", jsonObject2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject3;
    }

}