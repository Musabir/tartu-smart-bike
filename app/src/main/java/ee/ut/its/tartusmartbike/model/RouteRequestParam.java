package ee.ut.its.tartusmartbike.model;

public class RouteRequestParam {
    double lat;
    double lng;

    public RouteRequestParam(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public String toString() {
        return '{' +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
