package ee.ut.its.tartusmartbike.api;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import ee.ut.its.tartusmartbike.BuildConfig;
import ee.ut.its.tartusmartbike.model.DockListRequest;
import ee.ut.its.tartusmartbike.model.PlaceSearchListRequest;
import ee.ut.its.tartusmartbike.model.PlaceSearchResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PlaceSearchApi {

    private final OkHttpClient client;
    private final Gson gson = new Gson();
    private String query;

    public PlaceSearchApi(OkHttpClient client, String query) {
        this.client = client;
        this.query = query;
    }

    public List<PlaceSearchResult> all() throws IOException {
        Response response = client.newCall(generateRequest()).execute();
        String apiResponse = response.body().string();
        PlaceSearchListRequest resultList = this.gson.fromJson(apiResponse, PlaceSearchListRequest.class);
        return resultList.getSuggestions();
    }

    private Request generateRequest(){
        Request request = new Request.Builder()
                .url(BuildConfig.BUILD_TYPE+"&query="+query)
                .get().build();
        return request;
    }
}
