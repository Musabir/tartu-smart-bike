package ee.ut.its.tartusmartbike.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.util.ArrayList;
import java.util.List;

import ee.ut.its.tartusmartbike.R;
import ee.ut.its.tartusmartbike.common.Helper;
import ee.ut.its.tartusmartbike.model.RouteListModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.maps.Style.MAPBOX_STREETS;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

public class NavigationActivity extends AppCompatActivity implements OnMapReadyCallback, PermissionsListener {
    // variables for adding location layer
    private MapView mapView;
    private MapboxMap mapboxMap;
    // variables for adding location layer
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;
    // variables for calculating and drawing a route
    private DirectionsRoute currentRoute;
    private List<DirectionsResponse> originToOrigin;
    private List<DirectionsRoute> routes;
    private static final String TAG = "DirectionsActivity";
    private NavigationMapRoute navigationMapRoute;
    // variables needed to initialize navigation
    private RelativeLayout button;
    private RouteListModel currentElement;
    private List<DirectionsRoute> directionsRoutes;
    private SymbolManager symbolManager;
    private String originDockSymbolIconId = "originDockSymbolIconId";
    private String destinationDockSymbolIconId = "destinationDockSymbolIconId";
    private String destinationSymbolIconId = "destinationSymbolIconId";
    private Symbol destinationDockSymbol, originDockSymbol, destinationSymbol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_navigationsss);
        button = findViewById(R.id.startButton);
        Intent intent = getIntent();
        currentElement = (RouteListModel) intent.getExtras().getSerializable("currentElement");
        directionsRoutes = new ArrayList<>();
        directionsRoutes.add(currentElement.getSourceToDock());
        directionsRoutes.add(currentElement.getDestDockToDest());
        directionsRoutes.add(currentElement.getSourceDockToDestDock());
        getRoute(currentElement.getSourceToDock().routeOptions().coordinates().get(0),
                currentElement.getSourceToDock().routeOptions().coordinates().get(1),
                currentElement.getDestDockToDest().routeOptions().coordinates().get(0),
                currentElement.getDestDockToDest().routeOptions().coordinates().get(1));
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        routes = new ArrayList<>();
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(MAPBOX_STREETS, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                symbolManager = new SymbolManager(mapView, mapboxMap, style);
                enableLocationComponent(style);


                if (navigationMapRoute == null) {
                    navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.CustomNavigationMapRoute);
                }
                showRoutesInMap(style);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        boolean simulateRoute = true;
                        if (currentRoute != null) {
                            NavigationLauncherOptions options = NavigationLauncherOptions.builder()

                                    .directionsRoute(currentRoute)
                                    .shouldSimulateRoute(simulateRoute)
                                    .build();

                            NavigationLauncher.startNavigation(NavigationActivity.this, options);
                        } else
                            Toast.makeText(NavigationActivity.this, "Something goes wrong", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void showRoutesInMap(Style style) {
//        addMarkerToMap(style, "source-dock", currentElement.getSourceToDock().routeOptions().coordinates().get(1), "Source dock");
//        addMarkerToMap(style, "dest-dock", currentElement.getDestDockToDest().routeOptions().coordinates().get(0), "Destination dock");

        style.addImage(originDockSymbolIconId, ContextCompat.getDrawable(NavigationActivity.this, R.drawable.ic_marker_origin));
        style.addImage(destinationDockSymbolIconId, ContextCompat.getDrawable(NavigationActivity.this, R.drawable.ic_marker_destination));
        style.addImage(destinationSymbolIconId, ContextCompat.getDrawable(NavigationActivity.this, R.drawable.ic_destination));

        setupLayer(currentElement.getSourceToDock().routeOptions().coordinates().get(1), currentElement.getDestDockToDest().routeOptions().coordinates().get(0),
                currentElement.getDestDockToDest().routeOptions().coordinates().get(1));
        navigationMapRoute.addRoutes(directionsRoutes);
        //  navigationMapRoute.addRoute(currentElement.getSourceDockToDestDock());
    }

    private void setupLayer(Point originDock, Point destinationDock, Point destination) {

        originDockSymbol = symbolManager.create(new SymbolOptions()
                .withLatLng(new LatLng(originDock.latitude(), originDock.longitude()))
                .withIconImage(originDockSymbolIconId)
                .withIconSize(1.3f)
                .withIconOffset(new Float[]{0f, -1.5f})
                .withTextHaloColor("rgb(181, 55, 55)")
                .withTextHaloWidth(1.0f)
                .withTextSize(10f)
                .withTextColor("rgb(255, 255, 255)")
                .withTextAnchor("top")
                .withTextField("Origin dock")
                .withDraggable(false)
        );

        destinationDockSymbol = symbolManager.create(new SymbolOptions()
                .withLatLng(new LatLng(destinationDock.latitude(), destinationDock.longitude()))
                .withIconImage(destinationDockSymbolIconId)
                .withIconSize(1.3f)
                .withIconOffset(new Float[]{0f, -1.5f})
                .withTextHaloColor("rgb(50,205,50)")
                .withTextHaloWidth(1.0f)
                .withTextSize(10f)
                .withTextColor("rgb(255, 255, 255)")
                .withTextAnchor("top")
                .withTextHaloWidth(1.0f)
                .withTextField("Destination dock")
                .withDraggable(false)
        );

        destinationSymbol = symbolManager.create(new SymbolOptions()
                .withLatLng(new LatLng(destination.latitude(), destination.longitude()))
                .withIconImage(destinationSymbolIconId)
                .withIconSize(1.8f)
                .withIconOffset(new Float[]{0f, -1.5f})
                .withTextHaloColor("rgb(50,205,50)")
                .withTextHaloWidth(1.0f)
                .withTextSize(10f)
                .withTextColor("rgb(255, 255, 255)")
                .withTextAnchor("top")
                .withTextHaloWidth(1.0f)
                .withDraggable(false)
        );

    }

    private void getRoute(Point origin, Point middle1, Point middle2, Point destination) {
        NavigationRoute.Builder builder = NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .profile("cycling");
        builder.addWaypoint(middle1);
        builder.addWaypoint(middle2);
        builder.build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
// You can get the generic HTTP info about the response
                        Log.d(TAG, "Response code: " + response.code());
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(mapboxMap.getStyle());
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}