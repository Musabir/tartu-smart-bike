package ee.ut.its.tartusmartbike.model;

public class PointResponse {


    private String id;
    private double longitude;
    private double latitude;
    private String name;
    private String address;
    private Long totalLockedCycleCount;
    private Long electricBikes;
    private Long emptySlots;
    private Long mechanicalBikes;
    private String altitude;


    public PointResponse(String id, double longitude, double latitude, String name, String address, Long totalLockedCycleCount, Long electricBikes, Long emptySlots, Long mechanicalBikes) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
        this.name = name;
        this.address = address;
        this.totalLockedCycleCount = totalLockedCycleCount;
        this.electricBikes = electricBikes;
        this.emptySlots = emptySlots;
        this.mechanicalBikes = mechanicalBikes;
    }

    public PointResponse(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getTotalLockedCycleCount() {
        return totalLockedCycleCount;
    }

    public void setTotalLockedCycleCount(Long totalLockedCycleCount) {
        this.totalLockedCycleCount = totalLockedCycleCount;
    }

    public Long getElectricBikes() {
        return electricBikes;
    }

    public void setElectricBikes(Long electricBikes) {
        this.electricBikes = electricBikes;
    }

    public Long getEmptySlots() {
        return emptySlots;
    }

    public void setEmptySlots(Long emptySlots) {
        this.emptySlots = emptySlots;
    }

    public Long getMechanicalBikes() {
        return mechanicalBikes;
    }

    public void setMechanicalBikes(Long mechanicalBikes) {
        this.mechanicalBikes = mechanicalBikes;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    @Override
    public String toString() {
        return "PointResponse{" +
                "id='" + id + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", totalLockedCycleCount=" + totalLockedCycleCount +
                ", electricBikes=" + electricBikes +
                ", emptySlots=" + emptySlots +
                ", mechanicalBikes=" + mechanicalBikes +
                ", altitude='" + altitude + '\'' +
                '}';
    }
}
