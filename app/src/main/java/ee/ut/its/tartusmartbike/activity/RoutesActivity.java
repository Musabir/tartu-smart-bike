//package ee.ut.its.tartusmartbike.activity;
//
//import android.Manifest;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.Menu;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.Toast;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.google.gson.Gson;
//import com.google.gson.JsonObject;
//import com.here.android.mpa.common.GeoCoordinate;
//import com.here.android.mpa.common.GeoPolyline;
//import com.here.android.mpa.common.GeoPosition;
//import com.here.android.mpa.common.OnEngineInitListener;
//import com.here.android.mpa.common.PositioningManager;
//import com.here.android.mpa.mapping.AndroidXMapFragment;
//import com.here.android.mpa.mapping.Map;
//import com.here.android.mpa.mapping.MapMarker;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.lang.ref.WeakReference;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.ExecutionException;
//
//import ee.ut.its.tartusmartbike.BuildConfig;
//import ee.ut.its.tartusmartbike.R;
//import ee.ut.its.tartusmartbike.adapter.CustomPlaceSearchListAdapter;
//import ee.ut.its.tartusmartbike.api.DockApi;
//import ee.ut.its.tartusmartbike.api.PlaceSearchApi;
//import ee.ut.its.tartusmartbike.common.CustomAutoCompleteTextView;
//import ee.ut.its.tartusmartbike.model.DockListRequest;
//import ee.ut.its.tartusmartbike.model.PlaceSearchListRequest;
//import ee.ut.its.tartusmartbike.model.PlaceSearchResult;
//import ee.ut.its.tartusmartbike.services.DockService;
//import ee.ut.its.tartusmartbike.services.PlaceSearchService;
//import okhttp3.OkHttpClient;
//
//public class RoutesActivity extends AppCompatActivity {
//
//    private CustomAutoCompleteTextView originAddress,destinationAddress;
//    private CustomPlaceSearchListAdapter originPlaceSearchListAdapter,destinationPlaceSearchListAdapter;
//    private List<PlaceSearchResult> originPlaceAddressList,destinationPlaceAddressList;
//    private Gson gson;
//    private MapMarker originMarker, destinationMarker;
//
//
//    private AndroidXMapFragment mapFragment;
//    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
//    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
//            Manifest.permission.ACCESS_FINE_LOCATION,
//            Manifest.permission.ACCESS_COARSE_LOCATION,
//            Manifest.permission.ACCESS_WIFI_STATE,
//            Manifest.permission.ACCESS_NETWORK_STATE};
//    private Map map;
//    private GeoCoordinate currentPosition;
//    private PositioningManager positioningManager = null;
//    private PositioningManager.OnPositionChangedListener positionListener;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_routes);
//        currentPosition = new GeoCoordinate(58.3803565, 26.749944);
//        initializeMap();
//        originAddress = findViewById(R.id.origin_address);
//        destinationAddress = findViewById(R.id.destination_address);
//
//        originPlaceAddressList = new ArrayList<>();
//        destinationPlaceAddressList = new ArrayList<>();
//        gson = new Gson();
//
//        setAdapter();
//        textChangeListener(originAddress, originPlaceAddressList, originPlaceSearchListAdapter);
//        textChangeListener(destinationAddress, destinationPlaceAddressList,destinationPlaceSearchListAdapter);
//
//        initializeMarkers();
//        listItemOnClickListener(originAddress,originMarker);
//        listItemOnClickListener(destinationAddress,destinationMarker);
//
//    }
//
//    private void setAdapter(){
//        originPlaceSearchListAdapter = new CustomPlaceSearchListAdapter(this,
//                R.layout.adaper_custom_autocomplete, originPlaceAddressList);
//
//        destinationPlaceSearchListAdapter = new CustomPlaceSearchListAdapter(this,
//                R.layout.adaper_custom_autocomplete, destinationPlaceAddressList);
//
//        originAddress.setAdapter(originPlaceSearchListAdapter);
//        destinationAddress.setAdapter(destinationPlaceSearchListAdapter);
//    }
//
//    private void textChangeListener(CustomAutoCompleteTextView address, final List<PlaceSearchResult> list, final CustomPlaceSearchListAdapter adapter){
//        address.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (s.length() > 2) {
//                    sendAndRequestResponse(s.toString(), list,adapter);
//
//                }
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//    }
//
//    private void listItemOnClickListener(CustomAutoCompleteTextView address, final MapMarker mapMarker){
//        address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                PlaceSearchResult placeSearchResult = (PlaceSearchResult) parent.getItemAtPosition(position);
//                getLocationDetailsFromLocationID(placeSearchResult.getLocationId(),mapMarker);
//            }
//        });
//    }
//
//    private void getSearchResults(String query) {
//
//        try {
//            originPlaceAddressList.clear();
//            originPlaceAddressList.addAll(new PlaceSearchService(new PlaceSearchApi(new OkHttpClient(), query)).execute().get());
//            System.out.println("matchValues " + originPlaceAddressList.toString());
//            originPlaceSearchListAdapter = new CustomPlaceSearchListAdapter(this,
//                    R.layout.adaper_custom_autocomplete, originPlaceAddressList);
//            originAddress.setAdapter(originPlaceSearchListAdapter);
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main_menu, menu);
//        return true;
//    }
//
//    private void sendAndRequestResponse(String query, final List<PlaceSearchResult> list, final CustomPlaceSearchListAdapter adapter) {
//        String url = BuildConfig.PLACE_SEARCH_URL + "&query=" + query;
//        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
//        StringRequest mStringRequest = new StringRequest(Request.Method.GET, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        PlaceSearchListRequest resultList = gson.fromJson(response, PlaceSearchListRequest.class);
//                        list.clear();
//                        list.addAll(resultList.getSuggestions());
//                        adapter.notifyDataSetChanged();
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Log.i("---------><", "Error :" + error.toString());
//            }
//        });
//
//        mRequestQueue.add(mStringRequest);
//    }
//
//    private void getLocationDetailsFromLocationID(String locationId, final MapMarker mapMarker) {
//        String url = BuildConfig.LOCATION_ID_SEARCH_URL + "&locationid=" + locationId;
//        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
//        StringRequest mStringRequest = new StringRequest(Request.Method.GET, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                       parseAddress(response,mapMarker);
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Log.i("---------><", "Error :" + error.toString());
//            }
//        });
//
//        mRequestQueue.add(mStringRequest);
//    }
//
//    private void parseAddress(String response, MapMarker mapMarker){
//        try {
//            JSONObject jsonObj = new JSONObject(response);
//            JSONArray view = jsonObj.getJSONArray("view");
//            if(view.length()>0){
//               getLngLat(view,mapMarker);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//
//        }
//    }
//
//    private void getLngLat(JSONArray view, MapMarker mapMarker) throws JSONException {
//        double latitude = 0;
//        double longitude = 0;
//        JSONObject displayPosition = view.getJSONObject(0)
//                .getJSONArray("result").getJSONObject(0)
//                .getJSONObject("location").getJSONObject("displayPosition");
//        latitude = displayPosition.getDouble("latitude");
//        longitude = displayPosition.getDouble("longitude");
//
//        setMarkerToMap(latitude,longitude,mapMarker);
//    }
//
//    private void setMarkerToMap(double latitude, double longitude, MapMarker mapMarker){
//        mapMarker.setDraggable(true);
//        mapMarker.setCoordinate(new GeoCoordinate(latitude,latitude));
//        if(map!=null)
//            map.addMapObject(mapMarker);
//
//    }
//
//    private void initializeMap() {
//        mapFragment = (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
//        mapFragment.setRetainInstance(false);
//        mapFragment.init(new OnEngineInitListener() {
//            @Override
//            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
//                if (error == OnEngineInitListener.Error.NONE) {
//                    centralizeMap();
//                } else {
//                    Log.e(MainActivity.class.getCanonicalName(), error.getDetails());
//                    Toast.makeText(RoutesActivity.this, "Something goes wrong", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }
//
//    private void centralizeMap() {
//        map = mapFragment.getMap();
//        map.setCenter(currentPosition, Map.Animation.NONE);
//        map.setZoomLevel(14);
//        updatePositionInMap();
//        map.getPositionIndicator().setVisible(true);
//    }
//
//    private void updatePositionInMap() {
//        positioningManager = PositioningManager.getInstance();
//        positionListener = new PositioningManager.OnPositionChangedListener() {
//            @Override
//            public void onPositionUpdated(PositioningManager.LocationMethod method, GeoPosition position, boolean isMapMatched) {
//                currentPosition = position.getCoordinate();
//                map.setCenter(position.getCoordinate(), Map.Animation.NONE);
//            }
//
//            @Override
//            public void onPositionFixChanged(PositioningManager.LocationMethod method, PositioningManager.LocationStatus status) {
//            }
//        };
//
//        try {
//            positioningManager.addListener(new WeakReference<>(positionListener));
//            if (!positioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK)) {
//                Log.e("HERE", "PositioningManager.start: Failed to start...");
//            }
//        } catch (Exception e) {
//            Log.e("HERE", "Caught: " + e.getMessage());
//        }
//    }
//
//    private void initializeMarkers() {
//        originMarker = new MapMarker();
//        destinationMarker = new MapMarker();
//        originMarker.setDraggable(true);
//        destinationMarker.setDraggable(true);
//        originMarker.setDescription("origin");
//        destinationMarker.setDescription("destination");
//    }
//}
