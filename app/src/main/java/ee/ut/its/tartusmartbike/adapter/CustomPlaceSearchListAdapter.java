package ee.ut.its.tartusmartbike.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import ee.ut.its.tartusmartbike.R;
import ee.ut.its.tartusmartbike.model.PlaceSearchResult;

public class CustomPlaceSearchListAdapter extends ArrayAdapter {
    private List<PlaceSearchResult> dataList;
    private List<String> searchElements;
    private Context mContext;
    private int itemLayout;


    public CustomPlaceSearchListAdapter(Context context, int resource, List<PlaceSearchResult> storeDataLst) {
        super(context, resource, storeDataLst);
        dataList = storeDataLst;
        mContext = context;
        itemLayout = resource;

    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public PlaceSearchResult getItem(int position) {

        return dataList.get(position);
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {

        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(itemLayout, parent, false);
        }

        TextView strName = (TextView) view.findViewById(R.id.textView);
        strName.setText(getItem(position).getLabel());
        return view;
    }

}