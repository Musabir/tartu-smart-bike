package ee.ut.its.tartusmartbike.model;

import java.util.List;

public class PlaceSearchListRequest {
    private List<PlaceSearchResult> suggestions;

    public List<PlaceSearchResult> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<PlaceSearchResult> suggestions) {
        this.suggestions = suggestions;
    }
}
