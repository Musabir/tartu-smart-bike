package ee.ut.its.tartusmartbike.services;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ee.ut.its.tartusmartbike.api.DockApi;
import ee.ut.its.tartusmartbike.api.PlaceSearchApi;
import ee.ut.its.tartusmartbike.model.Dock;
import ee.ut.its.tartusmartbike.model.DockListRequest;
import ee.ut.its.tartusmartbike.model.PlaceSearchResult;

public class PlaceSearchService extends AsyncTask<Void, Void, List<PlaceSearchResult>> {

    private final PlaceSearchApi placeSearchApi;

    public PlaceSearchService(PlaceSearchApi placeSearchApi) {
        this.placeSearchApi = placeSearchApi;
    }

    public List<PlaceSearchResult> getSearchResult() throws IOException {
        final List<PlaceSearchResult> responses = placeSearchApi.all();
        return responses;
    }

    @Override
    protected List<PlaceSearchResult> doInBackground(Void... voids) {
        try {
            return this.getSearchResult();
        } catch (IOException e) {
            //todo handle the exception
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    protected void onPostExecute(List<PlaceSearchResult> searchResults) {


    }
}
