package ee.ut.its.tartusmartbike.model;


import com.mapbox.geojson.Point;

import java.io.Serializable;

public class SearchFieldResult implements Serializable {
    private String address;
    private Point point;

    public SearchFieldResult() {

    }

    public SearchFieldResult(String address, Point point) {
        this.address = address;
        this.point = point;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
