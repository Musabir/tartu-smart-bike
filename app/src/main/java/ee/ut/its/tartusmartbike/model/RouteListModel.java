package ee.ut.its.tartusmartbike.model;

import com.mapbox.api.directions.v5.models.DirectionsRoute;

import java.io.Serializable;

public class RouteListModel implements Serializable {
    private DirectionsRoute sourceToDock;
    private DirectionsRoute sourceDockToDestDock;
    private DirectionsRoute destDockToDest;
    private Long numOfElectricBike;
    private Long altitudeDiff;


    public RouteListModel() {

    }

    public RouteListModel(DirectionsRoute sourceToDock, DirectionsRoute sourceDockToDestDock, DirectionsRoute destDockToDest) {
        this.sourceToDock = sourceToDock;
        this.sourceDockToDestDock = sourceDockToDestDock;
        this.destDockToDest = destDockToDest;
    }

    public DirectionsRoute getSourceToDock() {
        return sourceToDock;
    }

    public void setSourceToDock(DirectionsRoute sourceToDock) {
        this.sourceToDock = sourceToDock;
    }

    public DirectionsRoute getSourceDockToDestDock() {
        return sourceDockToDestDock;
    }

    public void setSourceDockToDestDock(DirectionsRoute sourceDockToDestDock) {
        this.sourceDockToDestDock = sourceDockToDestDock;
    }

    public DirectionsRoute getDestDockToDest() {
        return destDockToDest;
    }

    public void setDestDockToDest(DirectionsRoute destDockToDest) {
        this.destDockToDest = destDockToDest;
    }

    public Long getNumOfElectricBike() {
        return numOfElectricBike;
    }

    public void setNumOfElectricBike(Long numOfElectricBike) {
        this.numOfElectricBike = numOfElectricBike;
    }

    public Long getAltitudeDiff() {
        return altitudeDiff;
    }

    public void setAltitudeDiff(Long altitudeDiff) {
        this.altitudeDiff = altitudeDiff;
    }

    @Override
    public String toString() {
        return "RouteListModel{" +
                "sourceToDock=" + sourceToDock +
                ", sourceDockToDestDock=" + sourceDockToDestDock +
                ", destDockToDest=" + destDockToDest +
                ", numOfElectricBike=" + numOfElectricBike +
                ", altitudeDiff=" + altitudeDiff +
                '}';
    }
}
