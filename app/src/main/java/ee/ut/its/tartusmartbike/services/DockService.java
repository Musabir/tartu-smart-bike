package ee.ut.its.tartusmartbike.services;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ee.ut.its.tartusmartbike.model.Dock;
import ee.ut.its.tartusmartbike.api.DockApi;
import ee.ut.its.tartusmartbike.model.DockListRequest;

public class DockService extends AsyncTask<Void, Void, List<DockListRequest.DockResponse>> {

    private final DockApi dockApi;

    public DockService(DockApi dockApi) {
        this.dockApi = dockApi;
    }

    public List<DockListRequest.DockResponse> getAllDocks() throws IOException {
        final List<DockListRequest.DockResponse> responses = dockApi.all();
        return responses;
    }

    private Dock toDock(DockListRequest.DockResponse response) {
        Dock dock = new Dock();
        dock.setName(response.getName());
        dock.setAddress(response.getAddress());
        dock.setLongitude(response.getAreaCentroid().getLongitude());
        dock.setLatitude(response.getAreaCentroid().getLatitude());
        return dock;
    }

    @Override
    protected List<DockListRequest.DockResponse> doInBackground(Void... voids) {
        try {
            return this.getAllDocks();
        } catch (IOException e) {
            //todo handle the exception
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    protected void onPostExecute(List<DockListRequest.DockResponse> docks) {


    }
}
